FROM ubuntu:17.10
MAINTAINER Juan Santisi "juan.santisi@ogsystems.com"

RUN apt-get update && \
       apt-get install -y --no-install-recommends \
       apt-transport-https \
       apt-utils \
       automake \
       build-essential \
       bzip2 \
       ca-certificates \
       clang-5.0 \
       cmake \
       curl \
       git \
       gcc-7 \
       g++-7 \
       libpapi-dev \
       libgeotiff-dev \
       libhdf5-dev \
       libhwloc-dev \
       libjemalloc-dev \
       libjpeg-dev \
       libmpich-dev \
       libopenexr-dev \
       libopenimageio-dev \
       libproj-dev \
       libtbb2 \
       libtbb-dev \
       libtiff5-dev \
       libtool \
       libwebp-dev \
       make \
       mercurial \
       mpich \
       pkg-config \
       software-properties-common \
       subversion \
       vim \
       wget \
       libcurl4-gnutls-dev \
       libpq-dev \
       postgresql-server-dev-all \
       libboost-all-dev \
       libopenmpi-dev \
       zlib1g-dev \
       cifs-utils \
       nfs-common \
       openssh-client \
       openssh-server \
       net-tools \
       sshpass && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

### OTF2 for HPX
RUN cd / && wget http://www.vi-hps.org/upload/packages/otf2/otf2-2.0.tar.gz
RUN cd / && tar -xzf otf2-2.0.tar.gz
RUN cd /otf2-2.0 && ./configure --prefix=/opt --enable-shared
RUN cd /otf2-2.0 && make -j8 install


RUN git clone git://github.com/STEllAR-GROUP/hpx.git && \
  cd hpx && \
	mkdir pf_hpx_build && \
  mkdir -p pf_hpx_build/share/hpx && \
	cd pf_hpx_build && \
	cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++-5.0 -DCMAKE_C_COMPILER=/usr/bin/clang-5.0 -DBOOST_ROOT=/usr/lib/x86_64-linux-gnu -DHPX_WITH_PARCELPORT_MPI=ON -DHPX_WITH_HWLOC=ON -DHWLOC_ROOT=/usr/include/hwloc -DHPX_WITH_MALLOC="jemalloc" -DJEMALLOC_ROOT=/usr/lib/x86_64-linux-gnu -DJEMALLOC_INCLUDE_DIR=/usr/include ../ && \
  make -j8 && \
 	make install

RUN cd / && rm -R hpx
